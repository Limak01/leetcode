//Link: https://leetcode.com/problems/set-matrix-zeroes/
//
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void set(vector<vector<int>>& matrix, int row, int col, pair<int, int> dir, vector<pair<int, int>>& changed) {
    if(row < 0 || row >= matrix.size() || col < 0 || col >= matrix[0].size()) {
        return;
    }

    if(matrix[row][col] != 0) {
        matrix[row][col] = 0;
        changed.push_back({row, col});
    }

    set(matrix, row + dir.first, col + dir.second, dir, changed);
}

void setZeroes(vector<vector<int>>& matrix) {
    int w = matrix[0].size(), h = matrix.size();
    vector<pair<int, int>> changed;

    for(int i = 0; i < h; i++) {
        for(int j = 0; j < w; j++) {
            if(matrix[i][j] == 0 && find(changed.begin(), changed.end(), (pair<int, int>){i, j}) == changed.end()) {
                set(matrix, i, j, {-1, 0}, changed); 
                set(matrix, i, j, {1, 0}, changed); 
                set(matrix, i, j, {0, 1}, changed); 
                set(matrix, i, j, {0, -1}, changed); 
            }
        }
    }
}

int main( void ) {
    vector<vector<int>> matrix = {{1,1,1},{1,0,1},{1,1,1}}; 

    setZeroes(matrix);

    for(auto x: matrix) {
        for(auto y: x) {
            std::cout << y << " ";
        }
        std::cout << std::endl;
    }
    return 0;
}

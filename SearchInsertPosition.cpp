//Link https://leetcode.com/problems/search-insert-position/
#include <iostream>
#include <vector>

using namespace std;

int searchInsert(vector<int>& nums, int target) {
    int index = -1;
    for(int i = 0; i < nums.size(); i++) {
        if(nums[i] == target) {
            index = i;
            break;
        }
    }

    if(index == -1) {
        for(int i = 0; i < nums.size(); i++) {
            if(i == nums.size() - 1 && nums[i] < target){
                index = i + 1;
                break;
            }

            if(i == 0 && nums[i] > target) {
                index = i;
                break;
            }

            if(nums[i] < target && nums[i + 1] > target) {
                index = i + 1;
                break;
            }
        }
    }

    return index;
}

int main() {
    vector<int> test = {1};
    cout << "Index: " << searchInsert(test, 0) << endl;
    return 0;
}
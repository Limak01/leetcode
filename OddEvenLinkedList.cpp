// Link https://leetcode.com/problems/odd-even-linked-list/

#include <iostream>

struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

ListNode* oddEvenList(ListNode* head) {
    if(head == NULL || head->next == NULL) return head;

    ListNode* even = head->next;
    ListNode* odd = head;
    ListNode* tmp = head;
    bool isOdd = true;

    ListNode* oddList = NULL;
    ListNode* evenList = NULL;

    while(tmp != NULL) {
        if(isOdd) {
            if(oddList == NULL) {
                oddList = tmp;
            } else {
                oddList->next = tmp;
                oddList = oddList->next;
            }
        } else {
            if(evenList == NULL) {
                evenList = tmp;
            } else {
                evenList->next = tmp;
                evenList = oddList->next;
            }
        }

        tmp = tmp->next;
        isOdd = !isOdd;
    }

    oddList->next = NULL;
    evenList->next = NULL;
    
    ListNode* tmp2 = odd;
    while(tmp2->next != NULL) {
        tmp2 = tmp2->next;
    }

    tmp2->next = even;
    
    return odd;
}

int main() {
    return 0;
}

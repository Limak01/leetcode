// Link https://leetcode.com/problems/reach-a-number/

#include <iostream>
#include <cmath>

using namespace std;

int reachNumber(int target) {
    target = abs(target);
    int sum=0,moves=0,i=1;

    while(true){
        sum+=i;
        i++;
        moves++;
        if(sum==target)return moves;
        if(sum>target && (sum-target)%2==0)return moves;
    }

    return moves;
}

int main() {
    cout << reachNumber(4);
    return 0;
}

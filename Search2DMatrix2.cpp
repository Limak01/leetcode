// Link https://leetcode.com/problems/search-a-2d-matrix-ii/

#include <iostream>
#include <vector>

using namespace std;

bool find(vector<int>& arr,  int target, int arr_size) {
    int left = 0, right = arr_size;

    while(left < right) {
        int mid = left + ((right - left) / 2);

        if(arr[mid] == target) return true;

        if(arr[mid] > target) {
            right = mid;
        }

        if(arr[mid] < target) {
            left = mid + 1;            
        }

    }

    return false;
}

bool searchMatrix(vector<vector<int>>& matrix, int target) {
    int m_Size = matrix.size();
    int r_Size = matrix[0].size();

    for(int i = 0; i < m_Size; i++) {
        if(target >= matrix[i][0] && target <= matrix[i][r_Size - 1]) {
            if(find(matrix[i], target, r_Size)) {
                return true;
            };
        }
    }

    return false;
}

int main() {
    vector<vector<int>> matrix = {{1,4,7,11,15},{2,5,8,12,19},{3,6,9,16,22},{10,13,14,17,24},{18,21,23,26,30}};

    int target = 5;

    cout << "Is in matrix: " << searchMatrix(matrix, target) << endl;
    return 0;
}

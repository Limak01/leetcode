//Link https://leetcode.com/problems/plus-one/submissions/
#include <iostream>
#include <vector>

using namespace std;
//  
//  9 9
// +  1
//    

vector<int> plusOne(vector<int>& digits) {
    int toNext = 0;
    vector<int> add = {};
    int sum = 0;

    for(int i = 0; i < digits.size(); i++) {
        if(i == digits.size() - 1) {
            add.push_back(1);
        } else {
            add.push_back(0);
        }
    }

    for(int i = digits.size() - 1; i >= 0; i--) {
        sum = digits[i] + add[i] + toNext;

        if(sum > 9) {
            toNext = sum / 10;
            digits[i] = sum % 10;
        } else {
            toNext = 0;
            digits[i] = sum;
        }
    }

    if(toNext != 0) {
        digits.insert(digits.begin(), toNext);
    }

    return digits;
}

int main() {
    vector<int> test = {9,9};

    vector<int> result = plusOne(test);
    
    for(auto number: result) {
        cout << number << endl;
    }
}

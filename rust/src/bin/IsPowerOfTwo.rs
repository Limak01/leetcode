// Link https://leetcode.com/problems/power-of-two/

fn is_power_of_two(n: i32) -> bool {
    if n == 0 {
        return false;
    }

    if n == 1 {
        return true;
    }

    let n_f: f64 = n.into();
    let log_n = n_f.log(2.0).round();
    
    2_f64.powf(log_n) == n as f64 
}

fn main() {
    println!("Result: {}", is_power_of_two(536870912));
}

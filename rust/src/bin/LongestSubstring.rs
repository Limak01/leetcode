// Link https://leetcode.com/problems/longest-substring-without-repeating-characters/

fn length_of_longest_substring(s: String) -> i32 {
    let s_as_vec: Vec<char> = s.chars().collect();
    let mut vec: Vec<char> = Vec::new();
    let mut i = 0;
    let mut result = 0;

    while i < s_as_vec.len() {
        for j in i..s_as_vec.len() {
            if vec.contains(&s_as_vec[j]) {
                if vec.len() > result {
                    result = vec.len(); 
                }

                vec.clear();
                break;
            }

            vec.push(s_as_vec[j]);
        }

        i += 1;
    }

    result as i32
}

fn main() {
    let test = length_of_longest_substring("abcabcbb".to_string());
    println!("Result: {}", test);
}

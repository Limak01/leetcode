// Link https://leetcode.com/problems/multiply-strings/
//
// So fucking weird

fn multiply(num1: String, num2: String) -> String {
    let n1;
    let n2;
    
    if num1.chars().count() > num2.chars().count() {
        n1 = num1;
        n2 = num2;
    } else {
        n1 = num2;
        n2 = num1;
    }

    if n2 == "0" {
        return "0".to_string();
    }

    //4256
    //  23

    let mut d = 0;
    let mut s: Vec<u32> = Vec::new();
    let mut result: Vec<u32> = Vec::new();
    let mut offset = 0;
    
    // Multiplying 
    for (i, num) in n2.chars().rev().enumerate() {
        for (j, num_two) in n1.chars().rev().enumerate() {
            let x: u32 = num.to_digit(10).unwrap();
            let y: u32 = num_two.to_digit(10).unwrap();
            let m = x * y + d;

            if m < 10 {
                s.insert(0, m);
                d = 0;
            } else {
                if j == n1.chars().count() - 1 {
                    s.insert(0, m);     
                } else {
                    s.insert(0, m % 10);
                }
                d = m / 10;
            }
        }
        
        for k in 0..offset {
            s.push(0);
        }

        

        if result.len() == 0 {
            result = s.clone();
        } else {
            let v1;
            let v2;
            let mut off = 0;
            let mut sum: Vec<u32> = Vec::new();

            if s.len() > result.len() {
                v1 = &s;
                v2 = &result;
            } else {
                v1 = &result;
                v2 = &s;
            }

            let mut it = v2.len() - 1;
            let mut tescik = 0;
            for number in v1.iter().rev() {
                if it as i32 - 1 < 0 {
                    if tescik == 0 {
                        let sm = number + v2[0] + off;
                        
                        if sm >= 10 {
                            sum.insert(0, sm % 10);
                            off = sm / 10;
                        } else {
                            sum.insert(0, sm);
                            off = 0;
                        }
                    } else {
                        sum.insert(0, *number + off);
                    }
                    tescik += 1;
                    continue;
                }
                
                let r = number + v2[it] + off;
                
                if r >= 10 {
                    sum.insert(0, r % 10);
                    off = r / 10;
                } else {
                    sum.insert(0, r);
                    off = 0;
                }

                it -= 1;
            }

            result = sum;
        }

        offset += 1;
        d = 0;
        s.clear();
    }

    
    result.into_iter().map(|i| i.to_string()).collect::<String>()
}

fn main() {
    let test = multiply("123".to_string(), "456".to_string());
    println!("{}", test);
}

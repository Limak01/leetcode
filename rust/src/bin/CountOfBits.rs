// Link https://leetcode.com/problems/counting-bits/

fn count_bits(n: i32) -> Vec<i32> {
    let mut result = Vec::new();
    
    for num in 0..=n {
        let mut t = num;
        let mut count = 0;

        while t != 0 {
            if t % 2 == 1 {
                count += 1;  
            }

            t = t / 2;
        }

        result.push(count);
    }

    result
}

fn main() {
    let test = count_bits(5);

    for i in test {
        println!("--> {}", i);
    }
}

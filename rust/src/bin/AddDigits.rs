// Link https://leetcode.com/problems/add-digits/

fn add_digits(num: i32) -> i32 {
    let num = num.to_string();
    let mut result: i32 = 0; 

    for n in num.chars() {
        result += n.to_digit(10).unwrap() as i32;
    }

    if result < 10 {
        return result;
    } 
    
    Solution::add_digits(result)
}

fn main() {
    let test = add_digits(38);

    println!("Result {}", test);
}

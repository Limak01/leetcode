// Link https://leetcode.com/problems/summary-ranges/

fn summary_ranges(nums: Vec<i32>) -> Vec<String> {
    let mut result = Vec::new();

    if nums.len() == 0 {
        return result;
    }

    if nums.len() == 1 {
        result.push(nums[0].to_string());
        return result;
    }

    let mut starting = nums[0];
    let mut prev = -1;

    for (i, &num) in nums.iter().enumerate() {
        if i == 0 {
            prev = num;
            continue;
        }

        if num - prev != 1 {
            if starting == (num - (num - prev)) {
                result.push(starting.to_string());
            } else {
                result.push(format!("{}->{}", starting, num - (num - prev)));
            }

            if i == nums.len() - 1 {
                result.push(num.to_string());
            }

            starting = num;
        } else if i == nums.len() - 1 {
            result.push(format!("{}->{}", starting, num));
        }


        prev = num;
    }

    result
}

fn main() { 
    let test = vec![0,2,3,4,6,8,9];

    let res = summary_ranges(test);

    for s in res {
        println!("{}", s);
    }
}

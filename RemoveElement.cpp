//Link https://leetcode.com/problems/remove-element/

#include <iostream>
#include <vector>

using namespace std;

int removeElement(vector<int>& nums, int val) {
    vector<int> result;

    for(auto x: nums) {
        if(x != val) 
            result.push_back(x);
    }        

    nums = result;

    return nums.size();
}

int main() {

    return 0;
}
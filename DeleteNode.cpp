// Link https://leetcode.com/problems/delete-node-in-a-linked-list/

#include <iostream>

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

void deleteNode(ListNode* node) {
    ListNode* prev = nullptr;

    while(node->next != nullptr) {
        int tmp = node->val;
        node->val = node->next->val;
        node->next->val = tmp;
        
        prev = node;
        node = node->next;
    }

    prev->next = nullptr;
    delete node;
}

// Simpler solution
void deleteNodeSimpler(ListNode* node) {
    node->val = node->next->val;
    node->next = node->next->next;
}

int main() {

    return 0;
}

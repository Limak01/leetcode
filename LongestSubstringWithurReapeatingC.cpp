// Link https://leetcode.com/problems/longest-substring-without-repeating-characters/
#include <iostream>
#include <algorithm>
#include <map>

using namespace std;

int lengthOfLongestSubstring(string s) {
    int start = 0;
    int end = 0;
    int max_size = 0;

    map<char, int> map;

   while(end < s.length()) {
       if(map.find(s[end]) != map.end()) {
            max_size = max(max_size, end - start);
            start = max(start, map[s[end]] + 1);
       }

       map[s[end]] = end;
       end++;
   }

    return max(max_size, end - start);
}

int main() {
    cout << "Length: " << lengthOfLongestSubstring("abcabcbb"); 
    return 0;
}

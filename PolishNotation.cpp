#include <iostream>
#include <stack>
#include <vector>

using namespace std;

int evalRPN(vector<string>& tokens) {
    stack<long int> stck;

    for(string i: tokens) {
        if(i == "-" || i == "+" || i == "*" || i == "/") {
            long int number1 = stck.top();
            stck.pop();

            long int number2 = stck.top();
            stck.pop();

            if(i == "-") 
                stck.push(number2 - number1);

            if(i == "+")
                stck.push(number1 + number2);

            if(i == "*")
                stck.push(number1 * number2);

            if(i == "/") 
                stck.push(number2 / number1);


        } else {
            long int number = stol(i);
            stck.push(number);
        }
    }

    return stck.top();
}

int main() {
    vector<string> test = {"2","1","+","3","*"};
    cout << evalRPN(test) << endl;
    return 0;
}

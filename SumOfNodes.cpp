// Link https://leetcode.com/problems/sum-of-nodes-with-even-valued-grandparent/
#include <iostream>

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

void traverse(TreeNode* node, TreeNode* grandparent, TreeNode* parent, int& result) {
    if(node == nullptr) return;
    
    if(grandparent != nullptr && grandparent->val % 2 == 0) {
        result += node->val;
    }

    traverse(node->left, parent, node, result);
    traverse(node->right, parent, node, result);
}

int sumEvenGrandparent(TreeNode* root) {
    int result = 0;
    traverse(root, nullptr, nullptr, result);

    return result;
}

int main() {

    return 0;
}

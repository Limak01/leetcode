// Link https://leetcode.com/problems/sqrtx/submissions/

#include <iostream>
#include <cmath>
//8
int mySqrt(unsigned int x) {
    long long left = 0;
    long long right = x;

    while(left <= right) {
        long long mid = std::floor(left + ((right - left) / 2));
        
        if(mid * mid == x) {
            return mid;
        } else if(mid * mid < x && (mid + 1) * (mid + 1) > x) {
            return mid;
        } else if(mid * mid > x) {
            right = mid - 1;
        } else {
            left = mid + 1;
        }
    }
    return left;
}

int main() {
    std::cout << mySqrt(16) << std::endl; 
}

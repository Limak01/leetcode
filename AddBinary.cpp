//Link https://leetcode.com/problems/add-binary/submissions/
#include <iostream>
#include <vector>

std::string addBinary(std::string a, std::string b) {
    std::vector<int> result = {};

    if(b.length() > a.length()) {
        std::string tmp = a;
        a = b;
        b = tmp;
    }
    
    if(a.length() != b.length()){

        int diff = a.begin() - b.begin();

        for(int i = 0; i < diff; i++) {
            b.insert(b.begin(), '0');
        }
    }


    std::vector<int> avec = {};
    std::vector<int> bvec = {};

    for(int i = 0; i < a.length(); i++) {
        avec.push_back(a[i] - '0');
        bvec.push_back(b[i] - '0');
    }
    
    int next = 0;
    
    for(int i = avec.size() - 1; i >= 0; i--) {
        int sum = avec[i] + bvec[i] + next;
        
        if(sum == 2) {
            result.insert(result.begin(), 0);
            next = 1;
        } else if(sum == 3) {
            result.insert(result.begin(), 1);
            next = 1;
        } else {
            result.insert(result.begin(), sum);
            next = 0;
        }
    } 
    
    if(next != 0) result.insert(result.begin(), next);
    std::string res = "";

    for(auto r: result) {
        res += std::to_string(r);
    }
    return res;
}

int main(){
    std::cout << addBinary("100", "110010") << std::endl;

}

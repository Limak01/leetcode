// Link https://leetcode.com/problems/contains-duplicate-ii/

#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

bool containsNearbyDuplicate(vector<int>& nums, int k) {
    unordered_map<int, int> m;
    
    for(int i = 0; i < nums.size(); i++) {
        if(m.find(nums[i]) != m.end()) {
            if(abs(m[nums[i]] - i) <= k) {
                return true;
            }
        }

        m[nums[i]] = i;
    }

    return false;
}

int main() {
    vector<int> nums = {1,2,3,1,2,3};
    bool test = containsNearbyDuplicate(nums, 2); 

    if(test) cout << "WOOW" << endl;
    else cout << "SADEQ" << endl;
    return 0;
}

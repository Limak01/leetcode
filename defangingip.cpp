//Link https://leetcode.com/problems/defanging-an-ip-address/
#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution {
public:
    string defangIPaddr(string address) {
        for(int i = 0; i < address.length(); i++) {
            if(address[i] == '.') {
                address.insert(i, "[");
                address.insert(i + 2, "]");
                i++;
            }
        }

        return address;
    }
};

int main() {
    Solution test;

    cout << test.defangIPaddr("255.100.50.0");
    return 0;
}

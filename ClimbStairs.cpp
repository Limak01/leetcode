#include <iostream>

int climbStairs(int n) {
    int ways = 1;
    //f(n)=f(n - 2)+f(n - 1)
    
    if(n == 1 || n == 2 || n == 3) return n;
    
    int nm2 = 2;
    int nm1 = 3;
    int tmp;

    for(int i = 4; i <= n; i++) {
        tmp = nm1 + nm2; 
        nm2 = nm1;
        nm1 = tmp;
    }

    return tmp;
}

int main() {
    std::cout << climbStairs(4) << std::endl;
}

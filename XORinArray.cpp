// Link https://leetcode.com/problems/xor-operation-in-an-array/

#include <iostream>
#include <vector>

using namespace std;

int xorOperation(int n, int start) {
    int result = 0;
    for(int i = 0; i < n; i++) {
        result  = result ^ (start + 2 * i); 
    }

    return result;
}

int main() {
    cout << xorOperation(5, 0);
    return 0;
}

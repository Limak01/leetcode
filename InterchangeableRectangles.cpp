//Link: https://leetcode.com/problems/number-of-pairs-of-interchangeable-rectangles/description/
#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;



long long factorial(int value) {
    if(value == 0) return 1; 
    
    return factorial(value - 1) * value;
}

long long interchangeableRectangles(vector<vector<int>>& rectangles) {
    unordered_map<double, int> mp;
    int size = rectangles.size();
    long long pairs = 0;
    
    for(int i = 0; i < size; i++) {
        mp[(double)rectangles[i][0] / (double)rectangles[i][1]]++;
    }

    for(pair<double, int> r: mp) {
        if(r.second > 1) {
            // N! / 2 * (N - 2)!  --->   N * (N - 1) * (N - 2)! / 2 * (N - 2)! --->  we shorten (N - 2)! and we get N * (N - 1) / 2
            long long result = (long long)r.second * ((long long) r.second - 1) / 2;
            pairs += result;
        }
    }

    return pairs;
}

int main( void ) {
    vector<vector<int>> rectangles = {{4,2},{1,3},{4,1},{4,2},{2,4},{1,1},{1,1}}; 
    
    std::cout << interchangeableRectangles(rectangles);

    return 0;
}

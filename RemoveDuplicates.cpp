//Link https://leetcode.com/problems/remove-duplicates-from-sorted-array/

#include <iostream>
#include <vector>

using namespace std;

//[0,0,1,1,1,2,2,3,3,4]

int removeDuplicates(vector<int>& nums) {
    vector<int> res;
    int pushed;



    for(int i = 0; i < nums.size(); i++) {
        if(i == 0) {
            pushed = nums[i];
            res.push_back(nums[i]);
            continue;
        };

        if(nums[i] != pushed) {
            res.push_back(nums[i]);
            pushed = nums[i];
        }
        
    } 

    nums = res;

    return nums.size();
}

int main() {
    vector<int> testCase = {0,0,1,1,1,2,2,3,3,4};
    int res = removeDuplicates(testCase);

    for(auto x: testCase) {
        cout << x << " ";
    }
    return 0;
}
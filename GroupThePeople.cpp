// Link https://leetcode.com/problems/group-the-people-given-the-group-size-they-belong-to/
#include <iostream>
#include <vector>

using namespace std;

// [2,1,3,3,3,2]
vector<vector<int>> groupPeople(vector<int>& nums) {
    vector<vector<int>> result;

    for(int i = 0; i < nums.size(); i++) {
        if(result.size() > 0) {
            bool pushed = false;
            for(int j = 0; j < result.size(); j++) {
                if(result[j][0] == nums[i] && result[j].size() < nums[i] + 1) {
                    result[j].push_back(i);
                    pushed = true;
                    break;
                }
            }

            if(!pushed) {
                vector<int> group;
                group.push_back(nums[i]);
                group.push_back(i);
                result.push_back(group);
            }
        } else {
            vector<int> group;
            group.push_back(nums[i]);
            group.push_back(i);
            result.push_back(group);
        }
    }
    
    for(vector<int>& vec: result) {
        vec.erase(vec.begin());
    }

    return result;
}

int main() {
    vector<int> c = {3,3,3,3,3,1,3};
    vector<vector<int>> test = groupPeople(c);

    for(auto x: test) {
        for(auto y: x) {
            cout << y;
        }
        cout << endl;
    }
    return 0;
}

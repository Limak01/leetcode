//Link https://leetcode.com/problems/implement-strstr/

#include <iostream>

using namespace std;

//helollewy
//ll

int strStr(string haystack, string needle) {
    if(needle.length() == 0) return 0;

    int index = -1;

    for(int i = 0; i < haystack.length(); i++) {
        if(haystack[i] == needle[0]) {
            index = i;
            for(int j = 0; j < needle.length(); j++) {
                if(haystack[i + j] == needle[j]) {
                    continue;
                } else {
                    index = -1;
                    break;
                }
            }

            if(index != -1) {
                break;
            }
        }
    }

    return index;
}

int main() {
    int test = strStr("baabbbbababbbabab","abbab");
    cout << test << endl;

    return 0;
}
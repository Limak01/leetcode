// Link https://leetcode.com/problems/majority-element/

#include <iostream>
#include <vector>

int majorityElement(std::vector<int>& nums) {
    float m = (float)nums.size() / 2;
    std::vector<int> checked;
    bool wasChecked = false;
    int count = 0;

    for(auto num: nums) {
        for(auto n: checked) 
            if(num == n) wasChecked = true;
        
        if(wasChecked) { 
            wasChecked = false;
            continue;
        }

        checked.push_back(num);

        for(auto n: nums) {
            if(n == num) count++;
        }

        if(count >= m) return num;

        count = 0;
        wasChecked = false;
    }
} 

int main() {

    return 0;
}

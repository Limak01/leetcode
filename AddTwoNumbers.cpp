// Link https://leetcode.com/problems/add-two-numbers/

#include <iostream>
#include <vector>

using namespace std;

struct ListNode {
    int val;
    ListNode *next;
 };

ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
    vector<int> vec_l1;
    vector<int> vec_l2;
    vector<int> result_vec;

    while(l1 != nullptr) {
        vec_l1.insert(vec_l1.begin(), l1->val);
        l1 = l1->next;
    }

    while(l2 != nullptr) {
        vec_l2.insert(vec_l2.begin(), l2->val);
        l2 = l2->next;
    }

    if(vec_l1.size() < vec_l2.size()) {
        vector<int> tmp = vec_l1;
        vec_l1 = vec_l2;
        vec_l2 = tmp;
    }

    while(vec_l2.size() != vec_l1.size()) {
        vec_l2.insert(vec_l2.begin(), 0);
    }

    int c = 0;

    for(int i = vec_l1.size() - 1; i >= 0; i--) {
        int sum = vec_l1[i] + vec_l2[i] + c;

        if(i != 0) {
            if(sum >= 10) {
                c = sum / 10;
                result_vec.insert(result_vec.begin(), sum % 10);
            } else {
                c = 0;
                result_vec.insert(result_vec.begin(), sum);
            }
        } else {
            if(sum >= 10) {
                result_vec.insert(result_vec.begin(), sum % 10);
                result_vec.insert(result_vec.begin(), sum / 10);
            } else {
                result_vec.insert(result_vec.begin(), sum);
            }
            
        }


    }


    string sum_as_str;

    for(int l: result_vec) {
        sum_as_str += l;
    }

    ListNode* result_head = nullptr;
    ListNode* prev = nullptr;

    for(int i = sum_as_str.length() - 1; i >= 0; i--) {
        ListNode* l = new ListNode;
        l->val = (int)sum_as_str[i];
        l->next = nullptr;
        
        if(i == sum_as_str.length() - 1) {
            result_head = l;
        }

        if(prev == nullptr) {
            prev = l;
        } else {
            prev->next = l;
            prev = prev->next;
        }
    }

    return result_head;
}

int main() {
    ListNode l1;
    ListNode l2;
    ListNode l3;

    l1.val = 9;
    l1.next = nullptr;
    l2.val = 9;
    l2.next = &l3;
    l3.val = 5;
    l3.next = nullptr;

    ListNode a1;
    ListNode a2;
    ListNode a3;
     ListNode a4;
    ListNode a5;
    ListNode a6;
    ListNode a7;
    ListNode a8;
    ListNode a9;
    ListNode a10;

    a1.val = 1;
    a1.next = &a2;
    a2.val = 9;
    a2.next = &a3;
    a3.val = 9;
    a3.next = &a4;
    a4.val = 9;
    a4.next = &a5;
    a5.val = 9;
    a5.next = &a6;
    a6.val = 9;
    a6.next = &a7;
    a7.val = 9;
    a7.next = &a8;
    a8.val = 9;
    a8.next = &a9;
    a9.val = 9;
    a9.next = &a10;
    a10.val = 9;
    a10.next = nullptr;

    ListNode* list1 = &l1;
    ListNode* list2 = &a1;

    ListNode* result = addTwoNumbers(list1, list2);

    while(result != nullptr) {
        cout << result->val << " ";
        result = result->next;
    }
}
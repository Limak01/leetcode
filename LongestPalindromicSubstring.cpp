// Link https://leetcode.com/problems/longest-palindromic-substring/

#include <iostream>

using namespace std;

string longestPalindrome(string s) {
    int s_Len = s.length();
    string longest = "";
    string actual = "";

    if(s_Len == 1) return s;

    for(int i = 0; i < s_Len; i++) {
        for(int j = i; j < s_Len; j++) {
            actual += s[j];

            int l = 0;
            int r = actual.length() - 1;
            bool isPalindrome = true;

            while(l < r) {
                if(actual[l] != actual[r]) {
                    isPalindrome = false;
                    break;
                }

                l++;
                r--;
            }

            if(isPalindrome && actual.length() > longest.length()) {
                longest = actual;
                cout << actual << endl;
            }
        }

        actual = "";
    }

    return longest;
}

int main() {
    longestPalindrome("cbbd"); 
    return 0;
}

//Link: https://leetcode.com/problems/maximum-number-of-moves-in-a-grid/
#include <iostream>
#include <vector>
#include <stack>

using namespace std;

pair<int, int> dirs[] = {
    {-1, 1},
    {0, 1},
    {1 , 1},
};

int move(int grid_rows, int grid_cols, vector<vector<int>>& grid, int row, int col, int prev_value) {
    if(row < 0 || row >= grid_rows || col < 0 || col >= grid_cols) { 
        return 0;
    }

    if(prev_value >= grid[row][col]) {
        return 0;
    }

    int val = grid[row][col];
    grid[row][col] = 0;

    int a = move(grid_rows, grid_cols, grid, row + 1, col + 1, grid[row][col]);
    int b = move(grid_rows, grid_cols, grid, row, col + 1, grid[row][col]);
    int c = move(grid_rows, grid_cols, grid, row - 1, col + 1, grid[row][col]);

    return 1 + max(a, max(b,c));

}

int maxMoves(vector<vector<int>>& grid) {
    int grid_rows = grid.size(), grid_cols = grid[0].size();
    int moves = 0;
    
    int i = 0;
    for(auto row: grid) {
        int tmp = move(grid_rows, grid_cols, grid, i, 0, -1);
        if(tmp > moves) moves = tmp - 1;
        i++;
    }

    return moves;
}

int main( void ) {
    vector<vector<int>> m = {{2,4,3,5},{5,4,9,3},{3,4,2,11},{10,9,13,15}};
    std::cout << maxMoves(m);    
    return 0;
}

// Link https://leetcode.com/problems/rotate-image/

#include <iostream>
#include <vector>

using namespace std;

void rotate(vector<vector<int>>& matrix) {
    vector<vector<int>> matrixCopy(matrix);
    int matrixSize = matrix.size();
    int column = matrix.size() - 1;
    int row = 0;

    for(int i = 0; i < matrixSize; i++) {
        for(int j = 0; j < matrixSize; j++) {
            matrix[row][column] = matrixCopy[i][j];
            row++;
        }
        row = 0;
        column--;
    }
}

int main() {
    vector<vector<int>> test = {{5,1,9,11},{2,4,8,10},{13,3,6,7},{15,14,12,16}};
    rotate(test); 

    for(int i = 0; i < test.size(); i++) {
        for(int j = 0; j < test[i].size(); j++) {
            cout << test[i][j] << " ";         
        }
        cout << endl;
    }
    return 0;
}

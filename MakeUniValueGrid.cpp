// Link https://leetcode.com/problems/minimum-operations-to-make-a-uni-value-grid/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int minOperations(vector<vector<int>>& grid, int x) {
    int gridSize = grid.size();
    int rowSize = grid[0].size();
    int result = 0;
    vector<int> sortedGrid;

    for(int i = 0; i < gridSize; i++) {
        for(int j = 0; j < rowSize; j++) {
            sortedGrid.push_back(grid[i][j]);
        }
    }

    sort(sortedGrid.begin(), sortedGrid.end());

    int mid = gridSize * rowSize / 2;

    for(int num: sortedGrid) {
        if(abs(sortedGrid[mid] - num) % x == 0) {
            result += abs(sortedGrid[mid] - num) / x;
        } else {
            return -1;
        }
    }

    return result;
}

int main() {
    vector<vector<int>> vec = {{1,5},{2,3}};
    
    cout << minOperations(vec, 1);    
    return 0;
}

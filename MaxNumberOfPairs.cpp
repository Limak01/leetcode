// Link https://leetcode.com/problems/max-number-of-k-sum-pairs/

#include <iostream>
#include <vector>
#include <unordered_map>
#include <algorithm>

using namespace std;

int maxOperations(vector<int>& nums, int k) {
    sort(nums.begin(), nums.end());
    int end = nums.size() - 1;
    int start = 0;
    int count = 0;
    
    while(start < end) {
        if(nums[start] + nums[end] == k) {
            count++;
            start++;
            end--;
        } else if (nums[start] + nums[end] < k) {
            start++;
        } else {
            end--;
        }
    }
    
    return count;
}

int main() {
    vector<int> n = {1,2,3,4};
    int count = maxOperations(n, 5);
    cout << "Count: " << count << endl;
    return 0;
}

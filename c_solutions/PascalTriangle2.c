// Link https://leetcode.com/problems/pascals-triangle-ii/submissions/

#include <stdio.h>
#include <stdlib.h>

int* getRow(int rowIndex, int* returnSize) {
    *returnSize = rowIndex + 1;
    int** buffer = (int**)malloc((rowIndex + 1) * sizeof(int*));

    for(int i = 0; i <= rowIndex; i++) {
        buffer[i] = malloc(sizeof(int) * (i + 1));
    }

    for(int i = 0; i <= rowIndex; i++) {
        if(i == 0) { buffer[0][0] = 1; continue; }

        for(int j = 0; j <= i; j++) {
            if(j == 0 || j == i) {
                buffer[i][j] = 1;
                continue;
            }

            buffer[i][j] = buffer[i - 1][j] + buffer[i - 1][j - 1];
        }
    }

    return buffer[rowIndex];
}

int main() {
    printf("%d", sizeof(int*));
    return 0;
}

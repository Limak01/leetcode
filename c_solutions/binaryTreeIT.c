// Link https://leetcode.com/problems/binary-tree-inorder-traversal/
#include <stdio.h>
#include <stdlib.h>

struct TreeNode {
    int val;
    struct TreeNode* left;
    struct TreeNode* right;
};

void inorder(struct TreeNode* root, int* iterator, int* buffer, int* size) {
    if(root == NULL) {
        return;
    }

    inorder(root->left, iterator, buffer, size);
    buffer[*iterator] = root->val;
    (*iterator)++;
    (*size)++;
    inorder(root->right, iterator, buffer, size);
}

int* inorderTraversal(struct TreeNode* root, int* returnSize) {
    int* buffer = malloc(100 * sizeof(int));
    int iterator = 0;
    int size = 0;
    
    inorder(root, &iterator, buffer, &size);
    
    *returnSize = size;
    return buffer;
}

int main() {
    struct TreeNode* root = malloc(sizeof(struct TreeNode));
    root->val = 1;
    struct TreeNode* node1 = malloc(sizeof(struct TreeNode));
    node1->val = 2;
    struct TreeNode* node2 = malloc(sizeof(struct TreeNode));
    node2->val = 3;

    root->right = node1;
    root->left = NULL;
    node1->left = node2;
    node1->right = NULL;
    node2->left = NULL;
    node2->right = NULL;
    
    int returnSize;

    int* test = inorderTraversal(root, &returnSize);
    for(int i = 0; i < returnSize; i++) {
        printf("%d \n", test[i]);
    }
   free(root);
    free(node1);
    free(node2);
    free(test);
    return 0;
}

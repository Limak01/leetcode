// Link https://leetcode.com/problems/number-of-1-bits/

#include <stdio.h>
#include <stdint.h>

int hammingWeight(uint32_t n) {
    int res = 0;

    while(n > 0) {
        if(n % 2 == 1) res++;
        n = n / 2;
    }

    return res;
}

int main() {

}

// Link https://leetcode.com/problems/binary-tree-preorder-traversal/

#include <stdio.h>
#include <stdlib.h>

void preorder(struct TreeNode* node, int* buffer, int* iterator) {
    if(node == NULL) return;

    buffer[*iterator] = node->val;
    (*iterator)++;
    preorder(node->left, buffer, iterator);
    preorder(node->right, buffer, iterator);
}

int* preorderTraversal(struct TreeNode* root, int* returnSize) {
    int* buffer = (int*)malloc(100 * sizeof(int));
    *returnSize = 0;

    preorder(root, buffer, returnSize);

    return buffer;
}

int main() {

    return 0;
}

// Link https://leetcode.com/problems/isomorphic-strings/submissions/

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

struct Map {
    char key;
    char value;
};

int containsKey(struct Map* arr, int arrLength, char key, char* checkFor) {
    if(strcmp(checkFor, "key") == 0) {
        for(int i = 0; i < arrLength; i++) {
            if(arr[i].key == key)
                return i;
        }

        return -1;
    } else {
        for(int i = 0; i < arrLength; i++) {
        if(arr[i].value == key)
            return i;
        }

        return -1;
    }
}


bool isIsomorphic(char* s, char* t) {
    if(strlen(s) != strlen(t)) return false;
    if(strcmp(s, t) == 0) return true;

    struct Map buffer[100];
    int bufferLength = 0;
    int slen = strlen(s);

    for(int i = 0; i < slen; i++) {
        if(containsKey(buffer, bufferLength, s[i], "key") == -1) {
            if(containsKey(buffer,bufferLength, t[i], "value") != -1) {
                return false;
            } else {
                struct Map mp;
                mp.key = s[i];
                mp.value = t[i];
                buffer[bufferLength] = mp;
                bufferLength++;
            }
        } else {
            int index = containsKey(buffer, bufferLength, s[i], "key");

            if(buffer[index].value != t[i]) {
                return false;
            }
        }
    }

    return true;
} 


int main() {
    bool test = isIsomorphic("paper", "title");

    if(test) {
        printf("ISOMORPHIC COS TAM");
    } else {
        printf("SADEQ");
    }
    return 0;
}

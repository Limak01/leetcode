// Link https://leetcode.com/problems/best-time-to-buy-and-sell-stock/submissions/

#include <stdio.h>

//[7,1,5,3,6,4]
int maxProfit(int* prices, int pricesSize) {
    int profit = 0;
    int buyDay = 0;

    for(int i = 0; i < pricesSize; i++) {
        if(prices[i] - prices[buyDay] > profit) {
            profit = prices[i] - prices[buyDay];
        } 

        if(prices[i] - prices[buyDay] < 0){
            buyDay = i;
        }

    }

    return profit;
}

int main() {
    int prices[6] = {7,1,5,3,6,4};

    printf("Profit: %d", maxProfit(prices, 6));
    return 0;
}

// Link https://leetcode.com/problems/contains-duplicate-ii/

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

typedef struct Node{
    int latest_index;
    int key;
    int val;
    struct Node *next;
} Node;

#define SIZE 1000
int getPos(int n){
    long long num=n;
    if(num<0)
        num=-1*num;
    return num%SIZE;
}

bool containsNearbyDuplicate(int* nums, int numsSize, int k){
    struct Node** hashMap=calloc(SIZE+1, sizeof(struct Node *));
    for(int i=0;i<numsSize;i++){
        int pos=getPos(nums[i]);
        struct Node* iter=hashMap[pos];
        struct Node* prev=hashMap[pos];
        while(iter){
            if(iter->key==nums[i]){
                if(iter->val>=1){
                    if(i <= iter->latest_index+k){
                        return true;
                    }
                }
                iter->latest_index=i;
                iter->val++;
                break;
            }
            prev=iter;
            iter=iter->next;
        }
        if(iter==NULL){
            struct Node *tmp=malloc(sizeof(struct Node));
            tmp->key=nums[i];
            tmp->val=1;
            tmp->latest_index=i;
            tmp->next=NULL;
            if(prev){
                prev->next=iter;
            }
            else{
                hashMap[pos]=tmp;
            }
        }
    }
    
    return false;
}

int main() {
    int arr[] = {1,2,3,1,44,22};
    int test = containsNearbyDuplicate(arr, 4, 3);

    return 0;
}

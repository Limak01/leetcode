// Link https://leetcode.com/problems/reverse-linked-list/
#include <stdio.h>

struct ListNode {
    int val;
    struct ListNode* next;
};

struct ListNode* reverseList(struct ListNode* head) {
    if(head == NULL) return head;

    struct ListNode* prev = NULL;
    struct ListNode* node = head;
    struct ListNode* next = head->next;

    while(node != NULL) {
        node->next = prev;
        prev = node;
        node = next;

        if(next != NULL)
            next = next->next;
    }

    return prev;
}

int main() {
    return 0;
}

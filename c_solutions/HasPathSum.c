// Link https://leetcode.com/problems/path-sum/submissions/

#include <stdio.h>
#include <stdbool.h>

bool check(struct TreeNode* root, int targetSum, int sum) {
    if(root == NULL) return false;
    if(sum + (root->val) == targetSum && root->left == NULL && root->right == NULL) return true;
    
       
    return check(root->left, targetSum, sum + root->val) || check(root->right, targetSum, sum + root->val);
}

bool hasPathSum(struct TreeNode* root, int targetSum){
    return check(root, targetSum, 0);
}

int main() {

    return 0;
}

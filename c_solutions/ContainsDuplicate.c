// Link https://leetcode.com/problems/contains-duplicate/

#include <stdio.h>
#include <stdbool.h>

void merge(int* inputArray, int* leftSide, int leftLength, int* rightSide, int rightLength) {
    int i = 0, j = 0, k = 0;
    while(i < leftLength && j < rightLength) {
        if(leftSide[i] <= rightSide[j]) {
            inputArray[k] = leftSide[i];
            i++;
        } else {
            inputArray[k] = rightSide[j];
            j++;
        }
        k++;
    }

    while(i < leftLength) {
        inputArray[k] = leftSide[i];
        i++;
        k++;
    }

    while(j < rightLength) {
        inputArray[k] = rightSide[j];
        j++;
        k++;
    }
}

void sort(int* array, int length) {
    if(length < 2) {
        return;
    }

    int mid = (length / 2);

    int leftSide[mid];
    int rightSide[length - mid];

    for(int i = 0; i < mid; i++) {
        leftSide[i] = array[i];
    }

    for(int i = mid; i < length; i++) {
        rightSide[i - mid] = array[i];
    }

    sort(leftSide, mid);
    sort(rightSide, length - mid);

    merge(array, leftSide, mid, rightSide, length - mid);
}

bool containsDuplicate(int* nums, int numsSize) {
    sort(nums, numsSize);

    for(int i = 0; i < numsSize - 1; i++) {
        if(nums[i] == nums[i + 1]) return true;
    }

    return false;
}

int main() {
    int nums[] = {4,1,33,55};
    bool test = containsDuplicate(nums, 4);

    return 0;
}

// Link https://leetcode.com/problems/happy-number/submissions/

#include <stdio.h>
#include <stdbool.h>
#include <math.h>

bool isHappy(int n) {
    int sum = 0;
    int num = n;
    int limit = 0;
    
    while(num != 1) {
        
        int iterator = 0;
        long int d = 10;

        while(1) {

            if(iterator == 0) {
                sum += pow(num % 10, 2);
                iterator++;
                continue;
            }
            
            if(num / d == 0) break;
            
            sum += pow((num / d) % 10, 2);
            iterator++;
            d *= 10;

        }

        if(sum == 0 || sum == 4) return false;

        num = sum;
        sum = 0;
        iterator = 0;
        limit++;
    }

    return true;
}


int main() {
    bool test = isHappy(1121); 
    
    if(test) {
        printf("HAPPY NUMBER");
    } else {
        printf("This number is not happy at all");
    }

    return 0;
}

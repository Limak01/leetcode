// Link https://leetcode.com/problems/remove-linked-list-elements/

#include <stdio.h>

struct ListNode {
    int val;
    struct ListNode* next;
};

struct ListNode* removeElements(struct ListNode* head, int val) {
    struct ListNode* prevNode = NULL;
    struct ListNode* node = head;

    while(node != NULL) {
        if(node->val == val) {
            if(prevNode == NULL) {
                head = node->next;
                node = node->next;
                continue;
            } else {
                node = node->next;
                prevNode->next = node;
                continue;
            }
        }

        prevNode = node;
        node = node->next;
    }

    return head;
}

int main() {
    return 0;
}

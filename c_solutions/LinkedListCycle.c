// Link https://leetcode.com/problems/linked-list-cycle/submissions/

#include <stdio.h>
#include <stdbool.h>

struct ListNode {
    int val;
    struct ListNode* next;
};

//2 -> 2 -> 5 -> 3 
bool hasCycle(struct ListNode* head) {
    if(head == NULL || head->next == NULL) return false;

    struct ListNode* node = head;
    struct ListNode* tmp = head;

    while(node != NULL) {
        if(node->next == tmp) return true;

        if(node == tmp) {
            node = node->next;
            tmp = head;
            continue;
        }

        tmp = tmp->next;
    }

    return false;
}

int main() {
    struct ListNode head;
    struct ListNode node1;
    struct ListNode node2;
    struct ListNode node3;

    head.val = 3;
    node1.val = 2;
    node2.val = 0;
    node3.val = -4;

    head.next = &node1;
    node1.next = &node2;
    node2.next = &node3;
    node3.next = &node1;

    bool test = hasCycle(&head);

    if(test) {
        printf("CYCLE");
    } else {
        printf("NO CYCLE");
    }

    return 0;
}

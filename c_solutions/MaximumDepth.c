// Link https://leetcode.com/problems/maximum-depth-of-binary-tree/

#include <stdio.h>
#include <stdlib.h>

struct TreeNode {
    int val;
    struct TreeNode* left;
    struct TreeNode* right;
};

int maxDepth(struct TreeNode* root) {
    if(root == NULL) {
        return 0;
    }
    
    int l = maxDepth(root->left);
    int r = maxDepth(root->right);

    if(l > r) {
        return 1 + l;
    } else {
        return 1 + r;
    } 
}

int main() {
    struct TreeNode root;
    struct TreeNode node1;
    struct TreeNode node2;
    struct TreeNode node3;
    struct TreeNode node4;

    root.val = 3;
    node1.val = 9;
    node2.val = 20;
    node3.val = 15;
    node4.val = 7;

    root.left = &node1;
    node1.left = NULL;
    node1.right = NULL;

    root.right = &node2;
    node2.left = &node3;
    node3.left = NULL;
    node3.right = NULL;
    node2.right = &node4;
    node4.left = NULL;
    node4.right = NULL;

    printf("%d", maxDepth(&root));

    return 0;
}

// Link https://leetcode.com/problems/single-number/

#include <stdio.h>
#include <stdbool.h>

int singleNumber(int* nums, int numsSize) {
    int x = 0;

    for(int i = 0; i < numsSize; i++) {
        x = x ^ nums[i];
    }

    return x;
}

int main() {
    int test[5] = {4,1,2,1,2};
    
    int res = singleNumber(test, 5);
    printf("\n\n\nResult: %d", res);
    return 0;
}

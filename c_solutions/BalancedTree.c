// Link https://leetcode.com/problems/balanced-binary-tree/submissions/

#include <stdio.h>
#include <stdbool.h>

int getHeight(struct TreeNode* node) {
    if(node == NULL) {
        return 0;
    }

    int l = getHeight(node->left);
    int r = getHeight(node->right);

    if(l > r) {
        return 1 + l;
    } else {
        return 1 + r;
    }
}

bool isBalanced(struct TreeNode* root) {
    if(root == NULL) {
        return true;
    }

    int l = getHeight(root->left);
    int r = getHeight(root->right);

    if(l - r <= 1) {
        return isBalanced(root->left) && isBalanced(root-right);
    } 

    if(r - l <= 1) {
        return isBalanced(root->left) && isBalanced(root-right);
    }

    if(r == l) {
        return isBalanced(root->left) && isBalanced(root->right);
    }
    
    return false;
}


int main() {

    return 0;
}

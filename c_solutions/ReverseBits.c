// Link https://leetcode.com/problems/reverse-bits/submissions/

#include <stdio.h>
#include <stdint.h>
#include <math.h>

uint32_t reverseBits(uint32_t n) {
    int result = 0;
    int j = 31;

    while(n > 0){
        if(n % 2 == 1) {
            result += pow(2, j);
        }
        n = n / 2;
        j--;
    }
    
    return result;
}


int main() {

    uint32_t test = reverseBits(43261596);

    printf("\n\n Result: %d", test);

    return 0;
}


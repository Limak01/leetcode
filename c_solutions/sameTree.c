// Link https://leetcode.com/problems/same-tree/submissions/
#include <stdio.h>

void checkNode(struct TreeNode* node1, struct TreeNode* node2, bool* same) {
    if((node1 == NULL && node2 != NULL) || (node1 != NULL && node2 == NULL)) {
        *same = false;
        return;
    }

    if(node1 == NULL && node2 == NULL) return;

    if(node1->val != node2->val) {
        *same = false;
        return;
    }

    checkNode(node1->left, node2->left, same);
    checkNode(node1->right, node2->right, same);
}

bool isSameTree(struct TreeNode* p, struct TreeNode* q) {
    bool same = true;
    
    checkNode(p, q, &same); 

    return same;
}

int main() {

    return 0;
}

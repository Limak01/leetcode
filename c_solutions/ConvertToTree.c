// Link https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/

#include <stdio.h>
#include <stdlib.h>

struct TreeNode* sortedArrayToBST(int* nums, int numsSize) {
    if(nums == NULL) {
        return NULL;
    }

    struct TreeNode* node;
    int left = 0;
    int right = numsSize - 1;
    int middle = left + ((right - left) / 2);
    int rightSize = right - middle;
    int leftSize = middle - left;
    int* rightNums;
    int* leftNums;
        
    if(rightSize == 0) {
        rightNums = NULL;
    } else {
        rightNums = malloc(rightSize * sizeof(int));
            
        for(int i = 0; i < rightSize; i++) {
            rightNums[i] = nums[(middle + 1) + i];
        }
    }
    
    if(leftSize == 0) {
        leftNums = NULL;
    } else {
        leftNums = malloc(rightSize * sizeof(int));
            
            
        for(int i = 0; i < middle; i++) {
            leftNums[i] = nums[i];
        }
    }
    
    
    node->val = nums[middle];
    node->left = sortedArrayToBST(leftNums, leftSize);
    node->right = sortedArrayToBST(rightNums, rightSize);

    free(rightNums);
    free(leftNums);

    return node;
    
}

int main() {

    return 0;
}

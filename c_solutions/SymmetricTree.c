// Link https://leetcode.com/problems/symmetric-tree/submissions/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct TreeNode {
    int val;
    struct TreeNode* left;
    struct TreeNode* right;
};

void inorder(struct TreeNode* node, int* buffer, int* iterator, int* size, bool side) {
    if(node == NULL) {
       buffer[*iterator] = (-202);
       (*iterator)++;
       (*size)++;
       return;
    }

    buffer[*iterator] = node->val;
    (*iterator)++;
    (*size)++;
   
    if(side) {
        inorder(node->left, buffer, iterator, size, side);
        inorder(node->right, buffer, iterator, size, side);
    } else {
        inorder(node->right, buffer, iterator, size, side);
        inorder(node->left, buffer, iterator, size, side);
    }
}

bool isSymmetric(struct TreeNode* node) {
    if(node == NULL) {
        return false;
    }

    int* buffer = malloc(1000 * sizeof(int));
    int* bufferTwo = malloc(1000 * sizeof(int));
    int iterator = 0;
    int iteratorTwo = 0;
    int size = 0;
    int sizeTwo = 0;
    bool same = true;

    inorder(node->left, buffer, &iterator, &size, true);
    inorder(node->right, bufferTwo, &iteratorTwo, &sizeTwo, false);

    if(size != sizeTwo) return false;

    for(int i = 0; i < size; i++) {
        if(buffer[i] != bufferTwo[i]) {
            same = false;
        }
    }
       
    free(buffer);
    free(bufferTwo);

    return same;
}

int main() {
    struct TreeNode root;
    root.val = 1;

    struct TreeNode node1;
    struct TreeNode node2;

    struct TreeNode node3;
    struct TreeNode node4;

    node1.val = 2;
    node2.val = 2;
    node3.val = 2;
    node4.val = 2;

    root.left = &node1;
    node1.left = &node2;
    node1.right = NULL;
    node2.right = NULL;
    node2.left = NULL;

    root.right = &node3;
    node3.left = &node4;
    node3.right = NULL;
    node4.left = NULL;
    node4.right = NULL;

    printf("\n \n \n Result: %d", isSymmetric(&root));

    return 0;
}

// Link https://leetcode.com/problems/palindrome-number/submissions/

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

bool isPalindrome(int x) {
    char* str;
    asprintf(&str, "%i", x);
    char* reversed = (char*)malloc(strlen(str) * sizeof(char));
    
    int iterator = 0;
    int differences = 0;
    for(int i = strlen(str) - 1; i >= 0; i--) {
        reversed[iterator] = str[i];
        iterator ++;
    }
    
    for(int i = 0; i < strlen(str); i++) {
        if(str[i] != reversed[i]) {
            differences++;
        }
    }
    if(differences == 0) {
        return true;
    } else {
        return false;
    }
}

int main() {
    isPalindrome(121); 
}

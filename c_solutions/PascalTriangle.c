// Link https://leetcode.com/problems/pascals-triangle/

#include <stdio.h>
#include <stdlib.h>

int** generate(int numRows, int* returnSize, int** returnColumnSizes) {
    int** resultArr = (int**)malloc(numRows * sizeof(int*));
    *returnColumnSizes = (int*)malloc(numRows * sizeof(int));
    *returnSize = numRows;
    
    // Memory allocation
    for(int i = 0; i < numRows; i++) {
        resultArr[i] = (int*)malloc((i + 1) * sizeof(int));
        (*returnColumnSizes)[i] = i + 1;
    }

    resultArr[0][0] = 1;

    for(int i = 1; i < numRows; i++) {
        for(int j = 0; j <= i; j++) {
            if(j == 0 || j == i) {
                resultArr[i][j] = 1;
                continue;
            }

            resultArr[i][j] = resultArr[i - 1][j] + resultArr[i][j - 1];
        }
    }

    return resultArr;
}

int main() {
    int returnSize;


    int** test = generate(5, &returnSize, returnColumnSizes);

    free(*test);
    return 0;
}

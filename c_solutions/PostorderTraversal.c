// Link https://leetcode.com/problems/binary-tree-postorder-traversal/

#include <stdio.h>
#include <stdlib.h>

void postorder(struct TreeNode* node, int* buffer, int* iterator) {
    if(node == NULL) return;

    postorder(node->left, buffer, iterator);
    postorder(node->right, buffer, iterator);
    buffer[*iterator] = node->val;
    (*iterator)++;
}

int* postorderTraversal(struct TreeNode* root, int* returnSize) {
    int* buffer = (int*)malloc(100 * sizeof(int));
    *returnSize = 0;
    
    postorder(root, buffer, returnSize);

    return buffer;
}

int main() {

    return 0;
}

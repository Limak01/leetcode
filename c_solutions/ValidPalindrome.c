// Link https://leetcode.com/problems/valid-palindrome/submissions/

#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>

bool isPalindrome(char* s) {
    int l = strlen(s);
    char buffer[l];
    int length = 0;
        
    for(int i = 0; i < l; i++) {
        if(isalnum(s[i])) {
            buffer[length] = (char)tolower(s[i]);
            length++;
        }
    }   
    
    if(length == 0) return true;
        
    
    int i = 0;
    int e = length - 1;
        
    while(e > i) {
        if(buffer[i++] != buffer[e--]) {
            return false;
        }
    }   
            
    return true;
}

int main() {
    bool test = isPalindrome("tat,txaaadsdas  :'rrat:tat"); 
    
    printf("%d", test);
    return 0;
}

// Link https://leetcode.com/problems/minimum-depth-of-binary-tree/
#include <stdio.h>

int minDepth(struct TreeNode* root) {
    if (root == NULL) {
        return 0;
    }

    int l = minDepth(root->left);
    int r = minDepth(root->right);
    
    if(l == 0 && r != 0) return 1 + r;
    if(r == 0 && l != 0) return 1 + l;

    if(l > r) {
        return 1 + r;
    } else {
        return 1 + l;
    }
}

int main() {

    return 0;
}

//Link: https://leetcode.com/problems/word-search/
#include <iostream>
#include <vector>

using namespace std;

bool check(vector<vector<char>>& board, string word, int word_index, int x, int y) {
    std::pair<int, int> up = {-1, 0}; 
    std::pair<int, int> left = {0, -1}; 
    std::pair<int, int> down = {1, 0}; 
    std::pair<int, int> right = {0, 1};

    if(word_index == word.length()) {
        return true;
    }

    if(x < 0 || x >= board.size() || y < 0 || y >= board[0].size()) {
        return false;
    }

    if(board[x][y] != word[word_index] || board[x][y] == -1) {
        return false;
    } else {
        board[x][y] = -1;
    
        if(check(board, word, word_index + 1, x + up.first, y + up.second) ||
                check(board, word, word_index + 1, x + left.first, y + left.second) ||
                check(board, word, word_index + 1, x + down.first, y + down.second) ||
                check(board, word, word_index + 1, x + right.first, y + right.second)) return true;

        board[x][y] = word[word_index];
        return false;
    }
}

bool exist(vector<vector<char>>& board, string word) {
    int h = board.size();
    int w = board[0].size();
    
    if(word.length() > (h * w)) return false;

    for(int i = 0; i < h; i++) {
        for(int j = 0; j < w; j++) {
            if(board[i][j] == word[0]) {
                if(check(board, word, 0, i, j)) return true;
            }
        }
    }

    return false;
}

int main(int argc, char** argv) {

    //["a","a","b","a","a","b"]
    //["a","a","b","b","b","a"]
    //["a","a","a","a","b","a"]
    //["b","a","b","b","a","b"]
    //["a","b","b","a","b","a"]
    //["b","a","a","a","a","b"]
    vector<vector<char>> board = {{'a','a','b','a','a','b'},{'a','a','b','b','b','a'},{'a','a','a','a','b','a'}, {'b','a','b','b','a','b'}, {'a','b','b','a','b','a'}, {'b','a','a','a','a','b'}};
    string word = "bbbaabbbbbab";

    if(exist(board, word)) {
        std::cout << "Exist" << std::endl; 
    }
     
    return 0;
}

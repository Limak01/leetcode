// Link https://leetcode.com/problems/string-without-aaa-or-bbb/

#include <iostream>

using namespace std;

string strWithout3a3b(int a, int b) {
    int resultLen = a + b;
    bool itsA = true;
    int aCount = 0;
    int bCount = 0;
    string result = "";

    if(b > a) {
        itsA = false;
    }

    for(int i = 0; i < resultLen; i++) {
        if(itsA && a > 0) {
            result += 'a';
            a--;
            aCount++;

            if(a == 0) itsA = false;
            if(a * 2 < b) itsA = false;
        } else if(!itsA && b > 0) {
            result += 'b';
            b--;
            bCount++;

            if(b == 0) itsA = true;
            if(b * 2 < a) itsA = true;
        }

        if(aCount == 2) {
            itsA = false;
            aCount = 0;
        }

        if(bCount == 2) {
            itsA = true;
            bCount = 0;
        }
    }

    return result;
}

int main() {
    cout << strWithout3a3b(3, 7);
    return 0;
}

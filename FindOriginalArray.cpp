// Link https://leetcode.com/problems/find-original-array-from-doubled-array/

#include <iostream>
#include <vector>
#include <algorithm>
#include <unordered_map>

using namespace std;

vector<int> findOriginalArray(vector<int>& changed) {
    if(changed.size() == 0) return {};
    if(changed.size() % 2 != 0) return {};

    unordered_map<int, int> mp;

    for(auto x: changed) {
        mp[x]++;
    }

    vector<int> result;

    sort(changed.begin(), changed.end());

    for(int el: changed) {
        if(mp.find(el * 2) != mp.end() && mp[el] != 0 && mp[el * 2] != 0) {
            mp[el]--;
            mp[el * 2]--;
            result.push_back(el);
        }
    }

    for(auto i: mp) {
        if(i.second != 0) {
            return {};
        }
    }
    return result;
}

int main() {
    vector<int> c = {6,3,0,1};
    vector<int> result = findOriginalArray(c);

    for(auto x: result) {
        cout << x << " ";
    }
    return 0;
}

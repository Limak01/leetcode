// Link https://leetcode.com/problems/excel-sheet-column-title/submissions/
#include <iostream>
#include <vector>

std::string convertToTitle(int columnNumber) {
    std::string result = "";

    while(columnNumber != 0) {
        int tmp = columnNumber % 26;
        if(tmp == 0) tmp = 26;

        result = (char)(tmp + 64) + result;
        columnNumber = (columnNumber - tmp) / 26;
    }

    return result;
}

int main() {
    std::cout << convertToTitle(2147483647) << std::endl; 

    return 0;
}

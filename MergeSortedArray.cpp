// Link https://leetcode.com/problems/merge-sorted-array/submissions/

#include <iostream>
#include <vector>

void merge(std::vector<int>& nums1, int m, std::vector<int>& nums2, int n) {
    int it = 0;

    for(int i = m; i < nums1.size(); i++) {
        nums1[i] = nums2[it];
        it++;
    }
    
    int swapped = 0;

    while(true) {
        for(int i = 1; i < nums1.size(); i++) {
            if(nums1[i - 1] > nums1[i]) {
                std::swap(nums1[i - 1], nums1[i]);

                swapped++;
            }
        }
        
        if(swapped == 0) {
            break;
        }
        swapped = 0;
    }
}

int main() {
    std::vector<int> a = {-1,0,0,3,3,3,0,0,0};
    std::vector<int> b = {1,2,2};

    merge(a, 6, b, 3);

    for(auto x: a) {
        std::cout << x << " " ;
    }
}

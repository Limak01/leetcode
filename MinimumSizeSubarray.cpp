// Link https://leetcode.com/problems/minimum-size-subarray-sum/description/

#include <iostream>
#include <vector>

using namespace std;

int minSubArrayLen(int target, vector<int>& nums) {
    int start = 0, end = 0;
    int min_size = -1;
    int sum = 0;
    int curr_size = 0;

    while(end != nums.size()) {
        sum += nums[end];
        curr_size++;
        end++;

        if(sum >= target) {
            if(min_size == -1) {
                min_size = curr_size;
            } else {
                min_size = min(min_size, curr_size);
            }

            while(start <= end) {
                sum -= nums[start];
                curr_size--;
                start++;

                if(sum >= target) {
                   min_size = min(min_size, curr_size);
                   continue;
                }

                break;
            }
        }
    }
    
    return min_size == -1 ? 0 : min_size;
}


int main() {
    vector<int> arr = {1,4,4};
    int test  = minSubArrayLen(4, arr);

    cout << "result: " << test << endl;
    return 0;
}

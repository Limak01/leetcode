// Link https://leetcode.com/problems/remove-duplicates-from-sorted-list/submissions/

#include <iostream>

struct ListNode {
    int value;
    ListNode* next;
};

ListNode* deleteDuplicates(ListNode* head) {
     ListNode* newHead = head;

    while(head != nullptr) {
        ListNode* prev = head;
        ListNode* tmp = head->next;
        
        // 1 1 2 3 3
        while(tmp != nullptr) {
            if(tmp->value == head->value) {
                ListNode* toDelete = tmp;
                tmp = tmp->next;
                prev->next = tmp;

                delete toDelete;
            } else {
                prev = prev->next;
                tmp = tmp->next;
            }
        }

        head = head->next;
    }

    return newHead;
}

int main() {
    ListNode* l1 = new ListNode;
    ListNode* l2 = new ListNode;
    ListNode* l3 = new ListNode;
    ListNode* l4 = new ListNode;
    ListNode* l5 = new ListNode;

    l1->value = 1;
    l1->next = l2;
    l2->value = 1;
    l2->next = l3;
    l3->value = 2;
    l3->next = l4;
    l4->value = 3;
    l4->next = l5;
    l5->value = 3;
    l5->next = nullptr;

    ListNode* head = deleteDuplicates(l1);
    while(head != nullptr) {
        std::cout << head->value << std::endl;
        head = head->next;
    }

    return 0;
}

/*Link https://leetcode.com/problems/merge-two-sorted-lists/ */

#include <iostream>

using namespace std;

struct ListNode {
    int val;
    ListNode *next;
};

ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
    if(list1 == nullptr && list2 == nullptr) return list1;

    if(list1 == nullptr && list2 != nullptr) 
        return list2;
    else if(list1 != nullptr && list2== nullptr)
        return list1;

    ListNode* hd = list1;
    while(list1->next != nullptr) {
        list1 = list1->next;
    }

    list1->next = list2;
    ListNode* mergedList = hd;

    int size = 0;
    while(mergedList != nullptr) {
        size++;
        mergedList = mergedList->next;
    }

    mergedList = hd;


    while(size > 0) {
        while(mergedList->next != nullptr) {
            if(mergedList->val > mergedList->next->val && mergedList->next != nullptr) {
                int tmp = mergedList->next->val;
                mergedList->next->val = mergedList->val;
                mergedList->val = tmp;
            }

            mergedList = mergedList->next;
        }
        mergedList = hd;
        size--;
    }

    return hd;
} 

int main() {
    ListNode l1;
    ListNode l2;
    ListNode l3;

    l1.val = 1;
    l1.next = &l2;
    l2.val = 2;
    l2.next = &l3;
    l3.val = 4;
    l3.next = nullptr;

    ListNode d1;
    ListNode d2;
    ListNode d3;

    d1.val = 1;
    d1.next = &d2;
    d2.val = 3;
    d2.next = &d3;
    d3.val = 4;
    d3.next = nullptr;

    ListNode* head1 = &l1;
    ListNode* head2 = &d1;


    ListNode* result = mergeTwoLists(head1, head2);

    while(result != nullptr) {
        cout << result->val << " ";
        result = result->next;
    }


    return 0;
}
//Link https://leetcode.com/problems/maximum-subarray/


#include <iostream>
#include <vector>

using namespace std;

//Kadane's algorithm
int maxSubarray(vector<int>& nums) {
	vector<int> sums = nums;
	int max = nums[0];

	for(int i = 1; i < nums.size(); i++) {
		if(sums[i] + sums[i - 1] > sums[i]) {
			sums[i] = sums[i] + sums[i - 1];
		}

		if(sums[i] > max) {
			max = sums[i];
		}
	}

	int sum = sums[0];

	for(int s: sums) {
		if(s > sum) {
			sum = s;
		}
	}

	return sum;
}

//Bruteforce, its working but it takes longer than Kadane's algorithm
int maxSubarrayBruteforce(vector<int>& nums) {
	int iteration = 0;
	int res;

	for(int i = 0; i < nums.size(); i++) {
		int sum = 0;
		for(int j = nums.size() - 1; j >= i; j--) {
			for(int k = i; k <= j; k++) {
				sum += nums[k];

			}

			if(iteration == 0) {
				res = sum;
				iteration++;
			} else if(sum > res) {
				res = sum;
			}
			sum = 0;
		}
	}

	return res;
}


int main() {
	vector<int> n = {-2,1,-3,4,-1,2,1,-5,4};

	cout << maxSubarray(n);
}
// Link https://leetcode.com/problems/maximum-points-you-can-obtain-from-cards/description/
#include <iostream>
#include <vector>

using namespace std;

int maxScore(vector<int>& cardPoints, int k) {
    int len = cardPoints.size();
    int left = 0;
    int right = len - k;
    int max_score = 0;
    int score = 0;
    
    for(int i = right; i < cardPoints.size(); i++) {
        score += cardPoints[i];
    }

    max_score = score;

    while(right < len) {
        score += cardPoints[left] - cardPoints[right];
        max_score = max(score, max_score);
        right++;
        left++;
    }

    return max_score; 
}

int main() {
    vector<int> test = {1,2,3,4,5,6,1};

    cout << maxScore(test, 3);
    return 0;
}

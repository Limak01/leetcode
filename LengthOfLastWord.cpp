//Link https://leetcode.com/problems/length-of-last-word/submissions/

#include <iostream>
#include <vector>

int lengthOfLastWord(std::string s) {
    std::vector<std::string> words = {};
    std::string lastWord = "";

    for(int i = 0; i < s.length(); i++) {
        lastWord += s[i];

        if(s[i] == ' ') {
            lastWord = "";
        } else {
            words.push_back(lastWord);
        }
    }
    
    return words[words.size() - 1].length();
}

int main() {
	std::string test = "   fly me   to   the moon  ";
   
    std::cout << lengthOfLastWord(test) << std::endl;
}

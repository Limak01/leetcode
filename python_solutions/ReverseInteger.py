# Link https://leetcode.com/problems/reverse-integer/

def reverse(x):
    asStr = str(x)[::-1]
    asStr = asStr.replace("-", "")

    if x < 0:
        asStr = "-" + asStr

    result = int(asStr)

    if result > 2**31 or result < -(2**31): 
        return 0

    return result

print(reverse(1534236469))

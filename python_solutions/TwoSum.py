def twoSum(nums, target):
    for i in range(0, len(nums)):
        for j in range(i, len(nums)):
            if nums[i] + nums[j] == target and j != i:
                return [i, j]

print(twoSum([3,2,4], 6))

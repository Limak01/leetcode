# Link https://leetcode.com/problems/powx-n/

def pow(x, n):
    if x == -1 and  n == -(2**31):
        return 1

    if x == 1 or x == -1:
        return x

    if n == 2**31 - 1 or n == -(2**31):
        return 0
        
    result = 1

    if n < 0:
        n = n * -1 
        x = 1 / x

    for _ in range(0, n):
        result *= x


    return result

print(pow(2, -2))

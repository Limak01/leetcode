def isPalindrome(x: int):
    s = str(x)
    arr = list(s)
    arr.reverse()
    reverse = "".join(arr)

    if reverse == str(x):
        return True

    return False


isPalindrome(23121)

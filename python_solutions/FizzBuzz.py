# Link https://leetcode.com/problems/fizz-buzz/

def fizzBuzz(n):
    result = []
    for i in range(1, n + 1):
        s = ""
        if i % 3 == 0:
            s += "Fizz"

        if i % 5 == 0:
            s += "Buzz"

        if len(s) == 0:
            s += str(i)

        result.append(s)

    return result



test = fizzBuzz(15)

print(test)


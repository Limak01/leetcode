//Link https://leetcode.com/problems/search-a-2d-matrix/

#include <iostream>
#include <vector>

using namespace std;

bool searchMatrix(vector<vector<int>>& matrix, int target) {
    int rowSize = matrix[0].size();
    int matrixSize = matrix.size();
    int index = 0;

    for(int i = 0; i < matrixSize; i++) {
        if(matrix[i][0] == target || matrix[i][rowSize - 1] == target) return true;

        if(target > matrix[i][0] && target < matrix[i][rowSize - 1]) {
            index = i;
            break;
        }
    }

    for(int num: matrix[index]) {
        if(num == target) {
            return true;
        }
    }

    return false;
}

int main() {
    vector<vector<int>> vec = {{1,3,5}};
    cout << searchMatrix(vec, 0) << endl;
    return 0;
}

// Link https://leetcode.com/problems/intersection-of-two-linked-lists/

#include <iostream>

ListNode* getIntersectionNode(ListNode* headA, ListNode* headB) {
    int lengthA = 0;
    int lengthB = 0;
    int diff;
    ListNode* tmpA = headA;
    ListNode* tmpB = headB;

    while(tmpA->next != NULL){
        lengthA++;
        tmpA = tmpA->next;
    }

    while(tmpB->next != NULL) {
        lengthB++;
        tmpB = tmpB->next;
    }

    if(lengthA > lengthB) {
        diff = lengthA - lengthB;
        while(diff != 0) {
            headA = headA->next;
            diff--;
        }
    }

    if(lengthB > lengthA) {
        diff = lengthB - lengthA;
        while(diff != 0) {
            headB = headB->next;
            diff--;
        }
    }

    while(headA != NULL) {
        if(headA == headB) return headA;

        headA = headA->next;
        headB = headB->next;
    }

    return NULL;
}

int main() {

    return 0;
}

// Link https://leetcode.com/problems/rotting-oranges/description/

#include <iostream>
#include <vector>

std::vector<std::pair<int, int>> get_rotten(std::vector<std::vector<int>>& grid) {
    int g_height = grid.size();
    int g_width = grid[0].size();
std::vector<std::pair<int, int>> result;

    for(int i = 0; i < g_height; i++) {
        for(int j = 0; j < g_width; j++) {
            if(grid[i][j] == 2) {
                result.push_back({ i, j });
            }
        }
    }

    return result;
}

bool check(std::vector<std::vector<int>>& grid) {
    int g_height = grid.size();
    int g_width = grid[0].size();

    for(int i = 0; i < g_height; i++) {
        for(int j = 0; j < g_width; j++) {
            if(grid[i][j] == 1) {
                return false;
            }
        }
    }

    return true;
}

int orangesRotting(std::vector<std::vector<int>>& grid) {
    int min = 0;
    std::vector<std::pair<int, int>> rotten = get_rotten(grid);
    std::vector<std::pair<int, int>> tmp;

    while(rotten.size() != 0) {
        for(std::pair<int ,int> r: rotten) {

            if(r.first - 1 >= 0 && grid[r.first - 1][r.second] == 1) {
                grid[r.first - 1][r.second] = 2;
                tmp.push_back({r.first - 1, r.second});
            }

            if(r.first + 1 < grid.size() && grid[r.first + 1][r.second] == 1) {
                grid[r.first + 1][r.second] = 2;
                tmp.push_back({r.first + 1, r.second});
            }

            if(r.second + 1 < grid[0].size() && grid[r.first][r.second + 1] == 1) {
                grid[r.first][r.second + 1] = 2;
                tmp.push_back({r.first, r.second + 1});
            }

            if(r.second - 1 >= 0 && grid[r.first][r.second - 1] == 1) {
                grid[r.first][r.second - 1] = 2;
                tmp.push_back({r.first, r.second - 1});
            }
        }

        if(tmp.size() != 0) min++;
        rotten = tmp;
        tmp.clear();
    }

    if(!check(grid)) {
        return -1;
    }

    return min;
}

int main(int argc, char** argv) {
     
    std::vector<std::vector<int>> test = {{2,1,1},{1,1,0},{0,1,1}};

    int result = orangesRotting(test);
    
    for(auto x: test) {
        for(auto y: x) {
            std::cout << y;
        }
        std::cout << std::endl;
    }

    std::cout << "Min: " << result << std::endl;

    return 0;
}

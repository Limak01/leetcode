//Link https://leetcode.com/problems/valid-parentheses/

#include <iostream>
#include <vector>

using namespace std;

bool isValid(string s);

int main() {
    bool t = isValid("{})");

    cout << t << endl;
    return 0;
}

bool isValid(string s) {
    
    if(s[0] == '}' || s[0] == ')' || s[0] == ']') return false;
    if(s.length() == 0 || s.length() == 1) return false;


    vector<char> v;

    for(auto x: s) {
        v.push_back(x);
    }


    while(v.size() != 0) {
            if(v[0] == '}' || v[0] == ')' || v[0] == ']') return false;
            for(int i = 0; i < v.size(); i++) {
                if(v[i] == '{') {
                    if(i == v.size() - 1) return false;

                    if(v[i + 1] != ']' && v[i + 1] != ')') {
                        if(v[i + 1] == '}') {

                            v.erase(v.begin() + (i));
                            v.erase(v.begin() + (i));
                            break;
                        } else continue;
                    } else return false;
                }

                if(v[i] == '(') {
                    if(i == v.size() - 1) return false;

                    if(v[i + 1] != ']' && v[i + 1] != '}') {
                        if(v[i + 1] == ')') {

                             v.erase(v.begin() + (i));
                            v.erase(v.begin() + (i));
                            break;
                        } else continue;
                    } else return false;
                }

                if(v[i] == '[') {
                    if(i == v.size() - 1) return false;

                    if(v[i + 1] != '}' && v[i + 1] != ')') {
                        if(v[i + 1] == ']') {

                             v.erase(v.begin() + (i));
                            v.erase(v.begin() + (i));
                            break;
                        } else continue;
                    } else return false;
                }
            }
    }

    return true;
}
// Link https://leetcode.com/problems/excel-sheet-column-number/

#include <iostream>
#include <cmath>

int titleToNumber(std::string columnTitle) {
    int length = columnTitle.length() - 1;
    int result = 0;
    int it = 0;
    int alph = 26;

    for(int i = length; i >= 0; i--) {
        result += ((int)columnTitle[i] - 64) * pow(alph, it); 
        it++;
    }
    
    return result;
}

int main() {
    int test = titleToNumber("FXSHRXW");

    std::cout << test << "\n";

    return 0;
}

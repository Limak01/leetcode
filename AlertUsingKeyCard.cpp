// Link https://leetcode.com/problems/alert-using-same-key-card-three-or-more-times-in-a-one-hour-period/

#include <iostream>
#include <vector>
#include <unordered_map>
#include <algorithm>

using namespace std;

bool compare(string a, string b) {
    return a < b;
}

vector<string> alertNames(vector<string>& keyName, vector<string>& keyTime) {
    unordered_map<string, vector<int>> useHours;
    vector<string> result;
    

    // Group key-card use by worker
    for(int i = 0; i < keyName.size(); i++) {
        int hours = stoi(keyTime[i].substr(0, keyTime[i].find(":"))) * 60;
        int minutes = stoi(keyTime[i].substr(keyTime[i].find(":") + 1, keyTime[i].length()));

        useHours[keyName[i]].push_back(hours + minutes);
    }
    
    // Sort key-card use for every worker
    for(auto &worker: useHours) {
        sort(worker.second.begin(), worker.second.end());
    }

    for(auto worker: useHours) {
        int count = 0;
        int min = 0;
        
        for(int i = 0; i < worker.second.size(); i++) {
            if(i + 2 < worker.second.size()) {
                int min = (worker.second[i + 2] - worker.second[i + 1]) + (worker.second[i + 1] - worker.second[i]);

                if(min <= 60) {
                    result.push_back(worker.first);
                    break;
                }
            }
        }
    }

    sort(result.begin(), result.end());

    return result;
}

int main() {
    vector<string> names = {"a","a","a","a","a","a","b","b","b","b","b"};
    vector<string> time = {"23:27","03:14","12:57","13:35","13:18","21:58","22:39","10:49","19:37","14:14","10:41"};

    vector<string> res = alertNames(names, time);
    cout << endl;
    for(auto name: res) {
        cout << name << endl;
    }
    return 0;
}

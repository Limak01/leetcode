// Link https://leetcode.com/problems/corporate-flight-bookings/
#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
    vector<int> corpFlightBookings(vector<vector<int>>& bookings, int n) {
        int bookings_len = bookings.size();
        
        vector<int> tmp(n + 1, 0);
        
        for(int i = 0; i < bookings_len; i++) {
            tmp[bookings[i][0] - 1] += bookings[i][2];
            tmp[bookings[i][1]] -= bookings[i][2];
        }

        vector<int> answer(n);
        answer[0] = tmp[0];

        for(int i = 1; i < n; i++) {
            answer[i] = answer[i - 1] + tmp[i];
        }

        return answer;
    }
};

int main() {
    Solution test;
    vector<vector<int>> b = {{1,2,10},{2,3,20},{2,5,25}};
    
    vector<int> result = test.corpFlightBookings(b, 5);
    for(auto t:result) {
        cout << t << " ";
    }
    return 0;
}
